# FaceNet Example
---
### Local FaceNet implementation.
*   Construct with Google’s trained neural network model FaceNet in Python.
*   Local storage with CSV file.
*   Scalable action list.
*   Logging.
*   Config.

## Environment
---
```
conda create --name myenv python=3.6.7
conda activate myenv
conda install -c conda-forge tensorflow==1.3.0
conda install -c conda-forge dlib==19.9.0
conda install -c conda-forge opencv==3.4.2
```
## Execution
---
```
activate myenv
python main.py
```
p.s. All the setting is in config.ini

## Structure
---
main.py

*   Initialize all Controllers and Datas with config.ini
*   Get all the availiable action from action.py

action.py

*   Actions(Functions) with @use_action **decorator** in this file can be selected when execution. 

Configs/ config.py

*   Read config from config.ini

Utils/ Argument_Util.py (Not used in this code)

*   Read argument input

Utils/ TypeConvert_Util.py

*   Types change like : image <=> base64

Datas/ FaceData.py

*   Deals with the Face Data
*   Load each data into **namedtuple** and append to a list
*   Transform the face image to base64, feature vectors to string and
*   Store the data in csv file

Controllers/ FaceController.py

*   Load the facenet Model
*   Calculate diff with the Face data.

Controllers/ ImgController.py

*   Camera control
*   Image preprocessing before facenet

## Version 1.0.1 Update
Structure       

*   Add LogController   
*   FaceData->DataController    

`main.py`       

*   Instead of import one by one, create Controllers/`__init__`.py to import

`config.ini`    

*   Add diff in Action section.

`Bug fix`       

*   Face ID error: Before, New ID is the current  data amount __NewID = len(FaceData)__, when delete the data will cause ID duplicate. After, __NewID = FaceData[-1].ID+1__

## Next Version
`Bug fix`   

*   Close windows by clicking 'X' in show image will cause program stuck.(while loop)   

`improve`   

*   Reverse log order.  
*   Use **with** statement to control camera    
*   Support SQL ...     


## FaceNet Model
>   https://github.com/movidius/ncappzoo/tree/master/tensorflow/facenet

>   https://github.com/davidsandberg/facenet