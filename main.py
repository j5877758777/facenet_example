# import config and all controllers
from Configs.config import MainConfig
from Controllers import *
# import actions
from action import action_list

import sys, os


def main():
    # Fix videoio(MSMF): OnReadSample() is called with error status: -1072873821
    os.environ["OPENCV_VIDEOIO_PRIORITY_MSMF"] = "0"
    
    # Initialize
    try:
        Logger = LogController(logger_name= MainConfig["logger_name"], logger_dir=MainConfig["logger_dir"])
        ImgHandler = ImgController(camera_num=MainConfig['camera_num'],
            camera_rotate=MainConfig['camera_rotate'])
        FaceHandler = FaceController()
        Faces = DataController()
    except Exception as e:
        Logger('ERROR', "{}:{}".format(e.__class__.__name__, e))
        print("ERROR: {}".format(e))
        return
    Logger('INFO', "FaceNet initialize success.")

    action_text = '\nEnter mode number '
    for i in range(len(action_list)):
        action_text += '{}) {} '.format(i, action_list[i].__name__)
    action_text += ':'

    func_args = {
        "FaceHandler": FaceHandler,
        "ImgHandler": ImgHandler,
        "Logger" : Logger,
        "Faces": Faces
    }

    # Select action
    while True:
        try:
            mode_num = input(action_text)
            mode_num = int(mode_num)
            if mode_num < 0 :
                raise IndexError
            action_list[mode_num](**func_args)
            Logger('INFO', "{} action success.".format(action_list[mode_num].__name__))
        except (ValueError, IndexError, KeyboardInterrupt) as e:
            '''
            ValueError: When user enter a none integer input.
            IndexError: When user enter an integer which is out of action_list range.
            KeyboardInterrupt : ctrl + c
            '''
            Logger('INFO', "FaceNet exit success.")
            Faces.save_data()
            sys.exit()
        except Exception as e:
            Logger('ERROR', "{}:{}".format(e.__class__.__name__, e))
            print("ERROR: {}".format(e))
            Faces.save_data()
            sys.exit()

if __name__ == '__main__':
    main()
