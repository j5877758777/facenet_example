from Configs.config import ImageConfig
import numpy as np
import cv2

class ImgController():
    '''
    Image(Frame) and Camera Streaming Controller.
    '''
    def __init__(self, camera_num = None, camera_rotate = None):
        icon_green_path = ImageConfig["icon_green_path"]
        icon_red_path = ImageConfig["icon_red_path"]
        icon_size = ImageConfig["icon_resize"]
        self.load_icon(icon_green_path, icon_red_path, icon_size)

        self.camera_rotate = camera_rotate
        if camera_num is None or camera_num == -1:
            self.availiable_camera_num = self.get_availiable_camera()
        else:
            if self.check_camera_availiable(camera_num = camera_num):
                self.availiable_camera_num = camera_num
            else:
                raise IOError("Camera number {} is not availiable.".format(camera_num))

    def get_availiable_camera(self, max_video_source_check=10):
        for i in range(max_video_source_check):
            if(self.check_camera_availiable(camera_num = i)):
                return i
        return -1

    def check_camera_availiable(self, camera_num):
        temp_cap = cv2.VideoCapture(camera_num)
        if temp_cap is None or not temp_cap.isOpened():
            temp_cap.release()
            return False
        temp_cap.release()
        return True

    def load_icon(self, icon_green_path, icon_red_path, resize_shape=(240, 240)):
        '''
        Read face icon into self.face_icon, face_red_icon with 3 channel.
        '''
        self.face_icon = cv2.imread(icon_green_path)
        self.face_red_icon = cv2.imread(icon_red_path)
        self.face_icon = cv2.resize(self.face_icon, resize_shape)
        self.face_red_icon = cv2.resize(self.face_red_icon, resize_shape)

        ''' Change color by code => the icon part have to be in the same color
        self.face_red_icon = copy.deepcopy(self.face_icon)
        self.face_red_icon[np.where((self.face_red_icon == [69,209,14]).all(axis = 2))] = [0,0,255]
        '''

    def open_camera(self):
        '''
        Open camera video stream.
        '''
        self.cap = cv2.VideoCapture(self.availiable_camera_num)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, ImageConfig["frame_width"])
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, ImageConfig["frame_height"])
        self.cap.set(cv2.CAP_PROP_FPS, ImageConfig["fps"])

        self.frame_height_middle_line = ImageConfig["frame_width"]/2
        self.frame_width_middle_line = ImageConfig["frame_height"]/2
          
    def close_camera(self):
        '''
        Release camera and destroy all cv2 windows.
        '''
        self.cap.release()
        cv2.destroyAllWindows()

    def mark_face_border(self, frame, det, text = None, color = (0, 255, 0)):
        cv2.rectangle(frame, (det.left() , det.top()),
            (det.right(), det.bottom()), color, 4, cv2.LINE_AA)
        
        if text is not None:
            self.img_put_text(frame, det.left(), det.top(), text)

    def img_put_text(self, frame, left, top, text):
        '''
        Put text on the frame img.
        '''
        cv2.putText(frame, text, (left , top), cv2.FONT_HERSHEY_DUPLEX,
            0.7, (223, 73, 17), 1, cv2.LINE_AA)

    def face_clip(self, frame, position, padding_size):
        '''
        Clip the face part of frame with padding.
        '''
        height, width, channels = frame.shape
        left = position.left() - padding_size
        if left < 0:
            left = 0
        
        right = position.right() + padding_size
        if right >= width:
            right = width-1

        top = position.top() - padding_size
        if top < 0:
            top = 0
        
        bottom = position.bottom() + padding_size
        if bottom >= height:
            bottom = height-1

        return frame[top:bottom, left:right]

    def show_image(self, image, name='Image'):
        '''
        Show image, Press any key to exit.

        Args:
            image: matrix
            name:  windows name, default: Image
        '''

        cv2.imshow(name, image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        return
    
    def horizontal_img(self, first_img, second_img):
        '''
        Merge two image horizontally.
        '''
        new_img = np.hstack((first_img, second_img))
        return new_img

    def img_overlay_center(self, main_img, overlay_img):
        '''
        Overlay the {overlay_img} on main_img.  
        We suggest overlay_img is smaller than main_img.
        '''
        margin_height = int((main_img.shape[0] - overlay_img.shape[0]) /2)
        margin_width = int((main_img.shape[1] - overlay_img.shape[1]) /2)
        
        # Target image ROI
        ROI = main_img[margin_height:margin_height+overlay_img.shape[0], margin_width:margin_width+overlay_img.shape[1], :]
        
        # Now create a mask of logo and create its inverse mask also
        img2gray = cv2.cvtColor(overlay_img,cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(img2gray,1,255,cv2.THRESH_BINARY)
        mask_inv = cv2.bitwise_not(mask)

        # The logo part with color.
        img1_bg = cv2.bitwise_and(overlay_img,overlay_img,mask=mask)

        # Take only region of logo from logo image.
        img2_fg = cv2.bitwise_and(ROI,ROI,mask=mask_inv)
        
        target_img = cv2.add(img1_bg, img2_fg)
        # Put logo in ROI and modify the main image
        alpha = 0.8
        dst = cv2.addWeighted(ROI, alpha, target_img, 1-alpha ,0)
        main_img[margin_height:margin_height+overlay_img.shape[0], margin_width:margin_width+overlay_img.shape[1], :] = dst
        
        return main_img

    def preprocess_image(self, src):
        '''
        ---
        * scale the image.
        * convert to RGB.
        * whiten.
        * reshape array.
        '''
        # scale the image
        NETWORK_WIDTH = 160
        NETWORK_HEIGHT = 160
        preprocessed_image = cv2.resize(src, (NETWORK_WIDTH, NETWORK_HEIGHT))

        #convert to RGB
        preprocessed_image = cv2.cvtColor(preprocessed_image, cv2.COLOR_BGR2RGB)

        #whiten
        preprocessed_image = self.whiten_image(preprocessed_image)

        preprocessed_image = np.reshape(preprocessed_image, (1, 160, 160 , 3)) 

        # return the preprocessed image
        return preprocessed_image
    
    def whiten_image(self, source_image):
        source_mean = np.mean(source_image)
        source_standard_deviation = np.std(source_image)
        std_adjusted = np.maximum(source_standard_deviation, 1.0 / np.sqrt(source_image.size))
        whitened_image = np.multiply(np.subtract(source_image, source_mean), 1 / std_adjusted)
        return whitened_image
    
    def camera_stream(self, frame_handler = None, **kwargs):
        '''
        Open Camera and Rotate frame,
        then send frame into frame_handler.

        Args:
            frame_handler: (Function),
            The first argument is frame, else argument input have to be kwargs.

            rcv = frame_handler(frame, **kwargs)

            if rcv is not None: close the camera and return rcv
            else : get the next frame and keep going 
        '''
        if frame_handler is None:
            return
        self.open_camera()
        
        while(self.cap.isOpened()):
            ret, frame = self.cap.read()
            if ret is False:
                # If frame can't read correctly.
                continue
                
            if self.camera_rotate != 0:
                frame = self.Rotate_Img(frame, angle = self.camera_rotate)

            rcv = frame_handler(frame, **kwargs)

            if rcv is not None:
                self.close_camera()
                return rcv

    def Rotate_Img(self, frame, angle):
        '''
        Rotate input np matrix.
        
        Args:
            frame(image): 3-dim np-matrix image.
            angle: Rotate angle(clockwise) e.x. 30 
        '''
        # grab the dimensions of the image and then determine the center
        (h, w) = frame.shape[:2]
        (cX, cY) = (w // 2, h // 2)
    
        # grab the rotation matrix (applying the negative of the
        # angle to rotate clockwise), then grab the sine and cosine
        # (i.e., the rotation components of the matrix)
        M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
        cos = np.abs(M[0, 0])
        sin = np.abs(M[0, 1])
    
        # compute the new bounding dimensions of the image
        nW = int((h * sin) + (w * cos))
        nH = int((h * cos) + (w * sin))
    
        # adjust the rotation matrix to take into account translation
        M[0, 2] += (nW / 2) - cX
        M[1, 2] += (nH / 2) - cY
    
        # perform the actual rotation and return the image
        return cv2.warpAffine(frame, M, (nW, nH), borderValue=(255, 255, 255)) # (nW, nH)