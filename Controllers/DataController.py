from Utils.TypeConvert_Util import img_to_transmit_b64, dim_to_string, b64_to_img, string_to_dim
from Configs.config import FaceDataConfig
import collections
import csv

Face = collections.namedtuple('Face', ['ID', 'Name', 'FaceImage', 'FaceVec'])

class DataController:
    '''
    Read/Write of Face Data.    
    Use namedtuple to store FaceData which is immutable.
    '''
    Data_path = FaceDataConfig["data_path"]
    Faces = []

    def __init__(self):
        try:
            self.read_data()
        except:
            print('CSV file is not exist. Initialize as empty data.')
        return

    def __len__(self):
        return len(self.Faces)

    def __getitem__(self, position):
        return self.Faces[position]

    def __delitem__(self, position):
        del self.Faces[position]

    def insertFace(self, face_tuple, index=None):
        if index is None: 
            self.Faces.append(face_tuple)
        else:
            self.Faces.insert(index, face_tuple)

    def find_index_by_key(self, key, value):
        '''
        Input: 
        key = 'ID', value = 19  
        Return the item index in the list , -1 => not found
        '''
        for index in range(len(self.Faces)):
            if getattr(self.Faces[index], key) == value:
                return index

        return -1
            
    def save_data(self):
        with open(self.Data_path, 'w', newline='') as csvfile:
            # 建立 CSV 檔寫入器
            writer = csv.writer(csvfile)

            # 寫入一列資料
            writer.writerow(['ID', 'Name', 'FaceImage', 'FaceVec'])

            for i in range(len(self.Faces)):
                writer.writerow([   str(self.Faces[i].ID),
                                    self.Faces[i].Name,
                                    img_to_transmit_b64(self.Faces[i].FaceImage),
                                    dim_to_string(self.Faces[i].FaceVec)
                ])
            
    def read_data(self):
        with open(self.Data_path, 'r', newline='') as csvfile:
            rows = csv.reader(csvfile)

            next(rows, None) # skip one line
            for row in rows:
                Current_Face = Face(    ID=int(row[0]),
                                        Name=row[1],
                                        FaceImage=b64_to_img(row[2]),
                                        FaceVec=string_to_dim(row[3])
                                        )
                self.Faces.append(Current_Face)
        return