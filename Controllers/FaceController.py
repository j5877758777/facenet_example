import Controllers.DL_Models.facenet_celeb_ncs.inception_resnet_v1 as network
from Configs.config import FaceConfig

# 2019-07-28 10:51:02.915984: W C:\tf_jenkins\...\tensorflow\core\platform\cpu_feature_guard.cc:45]
# The TensorFlow library wasn't compiled to use AVX2 instructions,
# but these are available on your machine and could speed up CPU computations.

# To get rid of TF warning. set TF_CPP_MIN_LOG_LEVEL = 2
# Someone says that the speed is almost the same. So currently I don't deal with it.
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import tensorflow as tf
import numpy as np
import dlib

class FaceController(object):
    '''
    Function
    ---
    * Initialize facenet
    * Detect Face
    * Calculate feature vectors
    * Compare two face
    * Get nearest face from a list of data
    '''
    def __init__(self, network_type='facenet'):
        '''
        Initialize the recognizer.
        '''
        self.network_type = network_type

        # get the detector of frontal face.
        self.detector = dlib.get_frontal_face_detector()

        try:
            if self.network_type == "facenet":
                self.Initialize_FaceNet()
            else:
                self.Initialize_dlib()
        except Exception as e:
            raise ImportError("Error on initializing {} network, error message: {}".format(self.network_type, e))

    def Initialize_FaceNet(self):
        image_size = 160 # Depend on the neural network, can't be change in this network.
        FaceNet_Graph = tf.Graph()
        with FaceNet_Graph.as_default():
            self.input_image_tensor = tf.placeholder("float", shape=[1, image_size, image_size, 3], name='input')
            prelogits, _ = network.inference(self.input_image_tensor, 1.0, phase_train=False)
            normalized = tf.nn.l2_normalize(prelogits, 1, name='l2_normalize')
            self.output_tensor = tf.identity(normalized, name='output')
            saver = tf.train.Saver(tf.global_variables())

        self.sess = tf.Session(graph = FaceNet_Graph)
        saver.restore(self.sess, FaceConfig["facenet_checkpoint_path"])

    def Initialize_dlib(self):
        # Load the shape_predictor_68_face_landmarks model.
        predictor_path = FaceConfig["predictor_path"]
        self.predictor = dlib.shape_predictor(predictor_path)
        # Load the face_recognition model.
        face_rec_model_path = FaceConfig["face_rec_model_path"]
        self.face_rec_model = dlib.face_recognition_model_v1(face_rec_model_path)

    def close_tensorflow(self):
        self.sess.close()
    
    def detect_face(self, img):
        '''
        Return the detector face data from img data.
        '''
        # Detect face position.
        dets = self.detector(img, 0)
        return dets
    
    def calculate_128_face_data(self, img, det = None):
        '''
        Calculate the 128 dimension face data array from detector face data.

        if network_type is set to dlib when initialize FaceController,
        you have to pass the det argument which return from detect_face.
        '''
        if self.network_type == "facenet":
            face_descriptor = self.sess.run(self.output_tensor, feed_dict={self.input_image_tensor:img})
            face_descriptor = np.reshape(face_descriptor, (128))
        else:
            # Get 68 face landmark
            self.shape = self.predictor(img, det)

            # Calculate the 128 dimension face data.
            face_descriptor = self.face_rec_model.compute_face_descriptor(img, self.shape)
        
        return face_descriptor
    
    def face_compare(self, data_1, data_2):
        '''
        Compare two img datas(128 dimension data) by  calculating L2-length
        '''
        diff = np.sqrt(np.sum(np.square(np.subtract(data_1, data_2))))
        return diff
    
    def get_nearest_face(self, target_data, dataset):
        '''
        Args:
            target_data: 128-dims target data.
        
        Return:
            diff: Minimum diff value(L2-error) in the database.
            index: The closest data index in the dataset.  
        '''
        min_diff = 2 # Usually 0.4 ~ 1.3, so instead of INT_MAX I set to 2
        min_index = -1

        for i in range(len(dataset)):
            current_diff = self.face_compare( dataset[i].FaceVec, target_data)
            if min_diff > current_diff:
                min_diff = current_diff
                min_index = i

        return min_diff, min_index