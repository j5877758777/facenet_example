from Controllers.DataController import Face
from Configs.config import ActionConfig
import cv2, copy

action_list = []
def use_action(func):
    print("Action name %s load" % func.__name__)
    action_list.append(func)
    return func

def fetch_face(frame, Frame_dict, ImgHandler, FaceHandler):
    '''
    Fetch face from camera  
    Args:
        frame: frame
        Frame_dict: Frame counter to count how long the face has stay in the camera, make sure get the correct face.  
        ImgHandler: ImgHandler
        FaceHandler: FaceHandler

    Return:
        face_128_data: Face 128-dims featured data from facenet.
        face_img : The cliped face image.
    '''
    dets = FaceHandler.detect_face(frame)
    _qualified_face_exist = False

    # For every face detected.
    for det in dets:
        current_face_size = det.right() - det.left()
        current_height_middle = int((det.right() + det.left())/2)
        current_width_middle = int((det.bottom() + det.top())/2)

        # Excepted Face
        if ActionConfig["max_face_size"] > current_face_size > ActionConfig["min_face_size"] and \
            abs(current_height_middle - ImgHandler.frame_height_middle_line) < ActionConfig["bias"] and \
            abs(current_width_middle - ImgHandler.frame_width_middle_line) < ActionConfig["bias"] :
            
            if Frame_dict["Stay_frame"] < ActionConfig["stay_threshold"]:
                # Stay less than {ActionConfig["stay_threshold"]} frame.
                ImgHandler.mark_face_border(frame, det, "Current size: {}".format(current_face_size))
                Frame_dict["Leave_frame"]   = 0
                Frame_dict["Stay_frame"]   += 1
                _qualified_face_exist = True
            else:
                # Fetch and return the face vector.
                face_img = ImgHandler.face_clip(frame=frame, position=det, padding_size=50)
                train_face_img = copy.deepcopy(face_img) # deepcopy a image used to the model.
                train_face_img = ImgHandler.preprocess_image(src = train_face_img)
                face_128_data = FaceHandler.calculate_128_face_data(train_face_img)
                return face_128_data, face_img
        else:
            ImgHandler.mark_face_border(frame, det, "Current size: {}".format(current_face_size))


    if _qualified_face_exist is False:
        # Overlay face_red_icon in the middle.
        frame = ImgHandler.img_overlay_center(frame, ImgHandler.face_red_icon)
        Frame_dict["Leave_frame"] += 1
        # If the frame doesn't have qualified face within {ActionConfig["Leave_threshold"]} frame, reset frame counter.
        if Frame_dict["Leave_frame"] >= ActionConfig["leave_threshold"]:
            Frame_dict["Leave_frame"]   = 0
            Frame_dict["Stay_frame"]    = 0
    else:
        # Overlay face_icon in the middle.
        frame = ImgHandler.img_overlay_center(frame, ImgHandler.face_icon)

    cv2.imshow("Face Camera", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        return "stop", "stop"

@use_action
def register(FaceHandler, ImgHandler, Faces, Logger, **dump_args):

    continue_flag = False

    # Enter Information
    while (not continue_flag):
        # ID = int(input('Enter ID number: '))
        Name = input('Enter name: ')
        ans = input('Name:{} y) Confirm n) No b) Back: '.format(Name))
        if(ans == 'y'):
            break
        elif(ans == 'b'):
            return
        else:
            continue

    # Catch Face
    while (not continue_flag):
        # Setup frame counter
        Frame_dict = {
            "Stay_frame": 0,
            "Leave_frame": 0,
        }
        face_128_vec, face_data = ImgHandler.camera_stream(
            frame_handler=fetch_face, Frame_dict=Frame_dict, FaceHandler=FaceHandler, ImgHandler=ImgHandler)

        # press q when catch face , will return "stop"
        if isinstance(face_128_vec, str) and face_128_vec == "stop":
            return

        cv2.imshow("Press y go to next step.", face_data)
        while True:
            press_key = cv2.waitKey(1)
            if press_key & 0xFF == ord('y'):
                cv2.destroyAllWindows()
                continue_flag = True
                break
            # elif press_key & 0xFF == ord('n'):
            elif press_key != -1:
                # -1: empty press key
                cv2.destroyAllWindows()
                break

    nextID = Faces[-1].ID+1 if len(Faces) != 0 else 0 
    face_tuple = Face(ID=nextID,
                      Name=Name,
                      FaceImage=face_data,
                      FaceVec=face_128_vec)

    Faces.insertFace(face_tuple=face_tuple)
    Logger('INFO', 'Face ID:{} Name:{} insert success'.format(face_tuple.ID, face_tuple.Name))
    return

@use_action
def recognize(FaceHandler, ImgHandler, Faces, Logger, **dump_args):
    Frame_dict = {
        "Stay_frame"        : 0,
        "Leave_frame"       : 0,
    }

    face_128_vec, face_data = ImgHandler.camera_stream(frame_handler=fetch_face, Frame_dict=Frame_dict, FaceHandler=FaceHandler, ImgHandler=ImgHandler)
    if isinstance(face_128_vec, str) and face_128_vec == "stop":
        return

    diff, index = FaceHandler.get_nearest_face(face_128_vec, Faces)

    # print("diff:{}".format(diff))
    Logger('INFO', 'Nearest Face ID:{} Name:{} Diff:{}'.format(Faces[index].ID, Faces[index].Name, diff))
    if diff < ActionConfig['diff']:
        cv2.imshow(Faces[index].Name, Faces[index].FaceImage)
        cv2.waitKey(0) # Wait until press any key.
        cv2.destroyAllWindows()
    else:
        print("Can't be recognize, please try to recognize again.")

@use_action
def show_data(Faces, **dump_args):
    '''
    Show all the ID and Name in the data.
    '''
    print("\n\tID\t Name")
    for i in range(len(Faces)):
        print("{}\t{}\t {}\n".format(i, Faces[i].ID, Faces[i].Name))

@use_action
def show_image(Faces, ImgHandler, **dump_args):
    '''
    Input ID and show the cliped face image. 
    '''
    while True:
        num = int(input('Enter image ID(-1 to exit): '))
        if num < 0:
            return
        try:
            index = Faces.find_index_by_key(key='ID', value=num)
            ImgHandler.show_image(image=Faces[index].FaceImage, name=Faces[index].Name)
        except Exception as e:
            print(e)

@use_action
def delete_data(Faces, Logger, **dump_args):
    '''
    Input ID and delete the face data. 
    '''
    while True:
        del_num = int(input('Enter delete ID(Enter -1 to exit): '))
        if del_num < 0:
            return
        try:
            index = Faces.find_index_by_key(key='ID', value=del_num)
            del_ID = Faces[index].ID
            del_NAME = Faces[index].Name
            del Faces[index]
            Logger('INFO', 'Face ID:{} Name:{} delete success'.format(del_ID, del_NAME))
        except Exception as e:
            print(e)

if __name__=='__main__':

    text = 'Select mode '
    for i in range(len(action_list)):
        text += '{}) {} '.format(i, action_list[i].__name__)
    print(text)
    