import configparser
import json
import os

config_path = "config.ini"

FaceDataConfig = dict()
ActionConfig = dict()
ImageConfig = dict()
FaceConfig = dict()
MainConfig = dict()


def load_config():
    '''
    Load config.ini into global variable.
    '''
    if os.path.exists(config_path):
        config = configparser.ConfigParser()
        config.read(config_path, encoding='utf-8-sig')
    else:
        raise IOError("Config file not found at '{}'.".format(config_path))

    global ImageConfig
    ImageConfig = dict(config['ImgController'])
    ImageConfig.update({
        'icon_resize': tuple([int(ImageConfig['icon_width']), int(ImageConfig['icon_height'])]),
        'frame_width': int(ImageConfig['frame_width']),
        'frame_height': int(ImageConfig['frame_height']),
        'fps': int(ImageConfig['fps'])
    })

    global FaceConfig
    FaceConfig = dict(config['FaceController'])

    global FaceDataConfig
    FaceDataConfig = dict(config['FaceData'])

    global ActionConfig
    ActionConfig = dict(config['Action'])
    ActionConfig.update({
        'diff': float(ActionConfig['diff']),
        'bias': int(ActionConfig['bias']),
        'min_face_size': int(ActionConfig['min_face_size']),
        'max_face_size': int(ActionConfig['max_face_size']),
        'stay_threshold': int(ActionConfig['stay_threshold']),
        'leave_threshold': int(ActionConfig['leave_threshold'])
    })

    global MainConfig
    MainConfig = dict(config['Main'])
    MainConfig.update({
        'camera_rotate': int(MainConfig['camera_rotate']),
        'camera_num': int(MainConfig['camera_num']),
    })

# Load Config
load_config()

if __name__ == '__main__':
    print(ImageConfig)
