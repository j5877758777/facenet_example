import base64
import numpy as np
import json
import cv2
from datetime import datetime

def dict_2_str(info_dict):
    return json.dumps(info_dict)

def str_2_dict(dict_string):
    return json.loads(dict_string)

def img_to_transmit_b64(img):
    '''
    Encode the image array in the type of jpg.
    Then encode to c# base64 string.
    '''
    retval, buffer = cv2.imencode('.jpg', img)
    img_b64 = base64.b64encode(buffer)
    binary_image = b64Img_2_b64String(img_b64)

    return binary_image

def b64Img_2_b64String(base64Img):
    '''
    Use on socket transport or API transport
    Input:
    ---
        "b'   base64_image   '"
    Return:
    ---
        "   base64_image   "
    
    '''
    img_string  = str(base64Img)
    # "b'   binary_image   '"
    b64_string_image =  img_string[2:-1]
    # "   binary_image   "

    return b64_string_image

def dim_to_string(dims_data):
    string = ""

    for i in range(dims_data.shape[0]):
        string += str(round(dims_data[i], 8))
        string += ","

    string  = string[:-1]
    
    return string

def string_to_dim(string):
    dims_data = string.split(",") # , num=string.count(str)
    return np.array(dims_data).astype(float)

def string_to_datetime(time_string):
    '''
    Args:
    ---
    time_string : 2018/8/14 17:03:33
    Return:
    ---
    datetime.datetime(2018, 8, 14, 17, 3, 33)
    '''
    return datetime.strptime(time_string, '%Y/%m/%d %H:%M:%S')

def datetime_to_string(date_time):
    '''
    Args:
    ---
    date_time = datetime.datetime(2018, 8, 14, 17, 3, 33)
    Return:
    ---
    [string] 2018/8/14 17:03:33
    '''
    return date_time.strftime('%Y/%m/%d %H:%M:%S')

def img_to_b64(img):
    '''
    Encode the image array in the type of jpg.
    Then encode to base64 string. 

    Can't directly convert to base64 string because ndarray is not C-contiguous.
    '''
    retval, buffer = cv2.imencode('.jpg', img)
    img_b64 = base64.b64encode(buffer)

    return img_b64

def b64_to_img(b64_img):
    '''
    From b64 image to image array.
    '''
    jpg_original = base64.b64decode(b64_img)
    jpg_as_np = np.frombuffer(jpg_original, dtype=np.uint8)
    image_buffer = cv2.imdecode(jpg_as_np, flags=1)

    return image_buffer

def img_to_file(img, path):
    '''
    Save image to file.
    '''
    cv2.imwrite(path, img)
    return 