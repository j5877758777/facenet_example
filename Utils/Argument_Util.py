import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--camera_num", default="1", help="Camera source number. Default is -1 auto-search for availiable camera, set to -2 not using camera.", type=int)
    parser.add_argument("--camera_angle", default="0", help="Camera rotate angle.", type=int)

    args = parser.parse_args()

    return args






